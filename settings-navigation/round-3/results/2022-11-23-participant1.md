# Participant 1, 2022-11-23

## Background

1. Uses 1 computer
2. Windows
3. Photo processing, email, chat, office tasks
4. 1 phone, running Android
5. Employed

## Exercise

1. About 

First guess is system. Is a little hesitant. "I can't see any that say 'my device', so I thought it might be to do with what the system is."

2. Users

Long pause. When prompted, guesses system again. Says that their second choice would be online accounts.

3. Wi-Fi 

Network. Quick answer, confident.

4. Date & Time

"Displays? I can't see one that mentions time, so I thought it might be displaying the time." I ask them to make a second choice. "Is it system? System relates to everything that's settings in the purest sense."

5. File Sharing

Network and internet - confident.

6. Remote Desktop

Long pause. I prompt them. "I'm wondering if it's privacy, but I would have expected that to be to do with preventing things." If you had to pick one? "Not sure... privacy?"

7. Remote Login

Skipped.

8. Media Sharing

Uncertain. "Is it sound...? I don't think so. Or it might be one of the applications. Otherwise, I'd try network and internet"

9. Default apps

"I might try system first." Asked to choose again: "maybe network or applications".

10. Region & Language

"Maybe appearance." Second choice: keyboard. "I know that in keyboard I can change the language of the keyboard, so maybe I could change the language there too. Or it could be system."
