# Participant 2, 2022-11-23

## Background

1. Class computers, 1 at home
2. Windows
3. Graphic Design, Office, Takes photos and edits them, Games, Video, Music, Email, Videos, Recipes
4. 1 iPhone
5. Employed, Geography Teacher, 56.

## Exercise

1. About 

System

2. Users

Online Accounts

3. Wi-Fi 

Network & Internet

4. Date & Time

System

5. File Sharing

Either Network or Applications, is shifting towards Network but thinks it "could be in the Gmail or Google Drive"

6. Remote Desktop

Long pause, "Online Accounts?", chose because they would have more knowledge, then chose System

7. Remote Login

Skipped.

8. Media Sharing

Bluetooth, because they've tried to make their TV play audio over Bluetooth, no other answers.

9. Default apps

Long pause, System, because it has a lot of subcategories. Nothing else.

10. Region & Language

Long pause, "System or Accessibility", because sometimes its also there on other systems. Maybe Keyboard.
