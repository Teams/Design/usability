## Background

1. Uses 2 computers
2. Fedora 37 (GNOME interface)
3. Office applications (Google Slides, Google Docs), occasional Games, Video calls, Online chat (Facebook Messenger), Watching videos, learning with Flashcards
4. 1 phone, running Android and 1 tablet, running iOS 15
5. The tested person is a student in the middle of high school

## Exercise

1. About 

System. Quick answer.

2. Users

First choice is Privacy - "I want to separate my data from my friend's data. He quickly rejected Online Accounts, because he supposes that the account isn't online. If he didn't find the setting, he still would be checking other tabs like System.

3. Wi-Fi 

Network & Internet. No problems.

4. Date & Time

Long thinking. Guided by intuition, finally selects "System".

5. File Sharing

Long thinking once again. He asks whether "on the same network" means devices connected to the same modem. After my response, he decided to go to "Network & Internet".

6. Remote Desktop

Quickly gives the response - Displays. He wants to connect his friend's display remotely. He is not eager to check a section other than Displays.

7. Remote Login

Skipped. 

8. Media Sharing

Bluetooth - tested person think that it works similarly to his Bluetooth headset and phone. Says his second choice is "Sound", assumes the output can be set there.

9. Default apps

After long thinking, decides on System, because the change feels very global, system-wide. His second choice is Apps. He wants to find his favorite browser in the list and set it as a default browser.

10. Region & Language

Quick answer - System. The System tab feels the most appropriate for the setting, out of the rest of tabs.
