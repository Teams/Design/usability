# Participant 3, 2022-12-19

## Background

1. Class computers, 1 laptop
2. Windows 10
3. Office, every day. Photo processing, rarely. Conferencing, sometimes. Music, very often. Email, very frequently. Chat. Videos (Movies).
4. 1 phone, Samsung A50
5. Teacher at schol.

## Exercise

1. About 

System, confident.

2. Users

Long pause, asked to repeat. Then Online accounts, confident.

3. Wi-Fi 

Network & Internet, confident.

4. Date & Time

Long pause, "System?"

5. File Sharing

Multitasking, or Network & Internet. Associated with giving people access.

6. Remote Desktop

Long pause. "Accesibility?" Associated access with accessibility.

7. Remote Login

Skipped.

8. Media Sharing

Long pause, Displays... or Bluetooth. Confused it with casting, instead of DLNA.

9. Default apps

Appearance, probably. Associated with changing the appearance of anything.

10. Region & Language

Long pause, "Appearance, maybe appearance or... appearance". "Oh sorry, system as well".
