# Participant 1, 2022-12-19

## Background

- 1 laptop, 1 desktop
- Windows 10, different versions
- Activities:
	- Office
	- Photo Processing (Rarely)
	- Video calls
	- Music
	- Email
	- Online Chat
	- Videos
	- 3D CAD and Modeling
- 1 Samsung Phone, A Series
- Student, 9th Grade

## Excercises
- About

System, confident

- Users

Thought online accounts, but backtracked, then said System.

- Wi-Fi

Network & Internet, confident

- Date & Time

System

- File Sharing

Online Accounts, because related to online sharing. Couldn't think of another answer.

- Remote Desktop

System

- Remote Login

Skipped

- Media Sharing

Displays, since you can project from Windows to a TV in the displays settings on Windows. No other answers.

- Default Apps

Applications, backtracked, then chose again.

- Region & Language

Appearance, connected appearance to language and everything visual. Second choice was system.
