# Card sort exercise, 2 October

## Background questions

How many desktop computers do you use on a regular basis? 1 (Windows 10)

What they use the computer for:

 - Office applications
 - Photo processing and managing
 - Playing games
 - Email
 - Online chat
 - Watching videos
 - Architecture projects

Phones/tablets: 1 phone, running iOS

Occupation: architecture student

## Exercise

1. Screen sharing

She quickly answered "Universal Access" because she understood from the word "Access" that there was something related to accessing her machine. Later she said that "Users" could be her try too because maybe there was something related to accessing her computer account.

2. Removable media

Picks "Applications".

3. Wi-Fi

Picks "Network & Internet"

4. About

Picks "About". She said that on her phone there is a similar option and whenever she needs to check something like that, she goes there.

5. Default apps

Picks "Applications".

6. Thunderbolt

She chose "Privacy", but I think it wouldn't be her first option if she understood what I explained her. I told her it was a sort of plug, but in Portuguese she may have misunderstood by something like an access. After she answered, I explained her again and used "USB" as an example and then she understood, but she didn't change her answer.

7. Keyboard shortcuts

She first thought about picking "Region & Language" because she thinks that there must be some "Keyboard" option there, but then her final choice was "Applications" because she believes that if she is looking for something to show all applications, there must be some setting there to enable a shortcut to open it.

8. Remote login
She picked "Monitors" because she thinks there must be something there where she can share her monitor or see it.

9. Screen lock

"Monitors" because she thinks it has something to do with her screen turning off, but later she thinks it has something to do with "Energy" because maybe it's related to power saving.

## Summary

| Task               | Test 3       |
|--------------------|--------------|
| Screen sharing     | Fail         |
| Removable media    | Pass         |
| Wi-Fi              | Pass         |
| About              | Pass         |
| Default apps       | Pass         |
| Thunderbolt        | Pass         |
| Keyboard shortcuts | Fail         |
| Remote login       | Fail         |
| Screen lock        | Fail         |

