# Card sort exercise, 2 October

## Background questions

How many desktop computers do you use on a regular basis? 1 (Ubuntu/ Windows 10 - dualboot)

What they use the computer for:

 - Office applications
 - Photo processing and managing
 - Playing games
 - Email
 - Online chat
 - Watching videos
 - Programming or software development
 - Architecture projects

Phones/tablets: 1 phone and 1 tablet, both running iOS

Occupation: software engineering student

## Exercise

1. Screen sharing

She picked "Privacy".

2. Removable media

Picks "Applications".

3. Wi-Fi

Picks "Network & Internet", she thinks maybe there is a list with her previously connected networks or her current network where she can click on one of them to see details and click an eye button to see the password.

4. About

Picks "About".

5. Default apps

Picks "Applications".

6. Thunderbolt

She looked for some hardware option related to thunderbolt (just like there are the "mouse", "monitor", "printer" options). She even thought for a moment on privacy, but she thinks privacy has more to do with passwords or data sharing preferences.

7. Keyboard shortcuts

She picked "Desktop & Shortcuts".

8. Remote login
She picked "Monitors" because she doesn't want her computer to be on sleep mode and then she'd go on "Privacy" to look for the remote session just like the first scenario.

9. Screen lock

Her first option was to go on "Monitors", but right after that she realized that it must be something related to "Power", although she thinks a "Battery & suspending" option would be clearer.

## Summary

| Task               | Test 4       |
|--------------------|--------------|
| Screen sharing     | Pass         |
| Removable media    | Pass         |
| Wi-Fi              | Pass         |
| About              | Pass         |
| Default apps       | Pass         |
| Thunderbolt        | Fail         |
| Keyboard shortcuts | Fail         |
| Remote login       | Fail         |
| Screen lock        | Fail         |

