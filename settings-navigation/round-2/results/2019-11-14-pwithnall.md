# Participant 1 (P), 2019-11-14
## Background

How many desktop computers do you use on a regular basis? 3 (OpenSUSE × 2, Windows 10 × 1)

What they use the computer for:

- Office applications
- Video calls or conferencing
- Email
- Programming or software development
- Watching videos

Phones/tablets: 2 × Android (from memory, versions 6 and lightweight 8)

Occupation: scientist (company director)

## Exercise

1. Keyboard shortcuts

They chose Keyboard with no difficulties.

2. Changing workspaces configurations

After a long pause, they said there was nowhere obvious. Could it be Displays? That’s more to do with monitors.
Could it be Users? As in, a preference for the current user.

No idea what multitasking is for. They think the top-level categorisation system in Settings should be for ‘system’ preferences vs ‘user’ preferences.

3. Changing keyboard layout

Confidently chose Keyboard.

After prompting about Region & Language, they agreed that would be a reasonable place to put the setting, but they’d see Region & Language more as a place for location and time settings.

4. Turning locations tracking off

Confidently chose Privacy.

If they didn’t find the setting there (or maybe if they did, just for a good measure of paranoia), they’d maybe also dig around Network & Internet, Bluetooth, Users, Online Accounts, or look in individual application settings within Applications.

5. Screen time

Notifications — they would expect to be able to set a notification to tell themselves to take a break.

After some thought, they said they’d also perhaps go to Displays and use that to set the screensaver to come on after a while (they forgot the fact that the computer needs to be idle for a screensaver to come on), but they acknowledge that would be a fudge.

Confidently said nowhere else.

After prompting about Screen Time, they said they supposed that made sense, but it didn’t register with them and wouldn’t be an obvious place for them to look.

6. Revoking camera access from some application

After a long pause, they said they’d look for a Camera section, like Displays/Keyboard/Mouse & Touchpad.

Given that there is no Camera section, they’d possibly look in Privacy, or possibly under Applications.

They were looking for where you’d set the permission on a per-app basis. The Privacy section, to them, feels like it’s about global settings rather than per-app settings.

7. Universal Access

Probably Universal Access, but if they didn’t find the setting there they’d also look under Displays or Sound.

We discussed what they understood by ‘universal access’, and they said “a way to adjust standard actions for non-standard users”.

8. Changing password

Easily picked Users.

9. Screen lock

Immediately picked Power — they said that locking the screen is one step in the process towards suspending the computer.

After some prompting, they said it could be in Displays since it just affects the screen.

After some more prompting, they agreed that Privacy would make sense, but it’s not the first place they’d look.

## Discussion points after the scenarios

 * They think it’s odd that some of the categories don’t seem to be as significant as the others. For example, they think Network & Internet is really important (used frequently), but then they’d never go into Date & Time.
 * They asked whether the list was prioritised by frequency of use.
 * They’d prefer to choose between ‘global’ or ‘per-app’ settings first, and then choose the setting category; rather than choosing the setting category and then choosing whether it applies globally or per-app.

## Summary

| Task                                          | Result             |
|-----------------------------------------------|--------------------|
| Keyboard shortcuts                            | :white_check_mark: |
| Changing workspaces configurations            | :x:                |
| Changing keyboard layout                      | :x:                |
| Turning locations tracking off                | :white_check_mark: |
| Screen time                                   | :x:                |
| Revoking camera access from some application  | :x:                |
| Universal Access                              | :white_check_mark: |
| Changing password                             | :white_check_mark: |
| Screen lock                                   | :x:                |

# Participant 2 (M), 2019-11-14
## Background

How many desktop computers do you use on a regular basis? 1 (Windows 10)

What they use the computer for:

- Office applications
- Photo processing and management
- Video calls or conferencing
- *Sometimes* listning to music
- Email
- Online chat
- Watching videos
- Other: MathCAD, AutoCAD

Phones/tablets: 2 × Android phones, 1 × shared access to an Android tablet

Occupation: civil engineer (full time)

## Exercise

1. Keyboard shortcuts

After a short pause, they chose Keyboard

2. Changing workspaces configurations

After a short pause, they chose Displays, because they want to change what they’re displaying.

They then thought about choosing Multitasking, since it “sounds like doing many things at once”.

3. Changing keyboard layout

Confidently chose Keyboard.

After some prompting, they also chose Region & Language, because this setting relates to the region they’re in.

4. Turning locations tracking off

Their immediate thought was “there’s no Location category”. They were unsure how to proceed.

They then said “I want to swipe down from the top [like a phone] and turn my location off”.

After some prompting, they said “[Privacy] is possibly a logical place to have it” — they didn’t think about that before, because they were thinking specifically about location.

5. Screen time

They thought about looking in Date & Time, because they were thinking about how they set an alarm via the clock on their phone.

They then thought about Notifications, so they could set themselves a notification to take a break. Then, “I’d probably just put it in Outlook tbh”.

After some prompting, they said that they guess Screen Time is about limiting the time using a screen, but that they hadn’t clocked that category at all.

6. Revoking camera access from some application

Immediately chose Applications.

They said that if they didn’t find it there, they’d look in Privacy.

They would also consider just physically putting some tape over the camera.

They said that the Applications category sounds like it’s about per-app settings, including access rights, like on phones.

7. Universal Access

They immediately chose Notifications, but said that if they didn’t find the setting there, they’d look in Displays, or Sound.

After some prompting, they said they would look in Universal Access if they didn’t find the setting elsewhere. Universal Access does indeed seem to be “about access for all”.

8. Changing password

Their immediate thought was to use `Ctrl`+`Alt`+`Del` and change the password that way.

They then chose Users, but they said they looked for an ‘Accounts’ section first.

9. Screen lock

They thought about going to Displays, but said that screen locking is about more than just the display going to sleep.

They then thought about going to Users, since they would want the setting to just be for them (and not other users — i.e. they thought the Users section was for settings specific to each user).

Maybe the Power section?

After prompting, they agreed that Privacy probably did make sense, since you want your computer to be private when you’re not there. But it’s not where they would look.

## Discussion points after the scenarios

 * They generally ignore settings and configuration on their computer, as the IT office in their company sort them out for people.

## Summary

| Task                                          | Result             |
|-----------------------------------------------|--------------------|
| Keyboard shortcuts                            | :white_check_mark: |
| Changing workspaces configurations            | :question:         |
| Changing keyboard layout                      | :x:                |
| Turning locations tracking off                | :x:                |
| Screen time                                   | :x:                |
| Revoking camera access from some application  | :white_check_mark: |
| Universal Access                              | :x:                |
| Changing password                             | :white_check_mark: |
| Screen lock                                   | :x:                |

# Participant 3 (C), 2019-11-14
## Background

How many desktop computers do you use on a regular basis? 2 (Windows (10?), Chromebook)

What they use the computer for:

- Graphic design or digital artwork
- Office applications
- Photo processing and management
- Listening to music
- Email
- Watching videos

Phones/tablets: 2 × Android phones, 1 × iPad

Occupation: charity sector (self-employed)

## Exercise

1. Keyboard shortcuts

They would look in the Search category first, because they wanted to search for the setting.

Then they would look in Applications, since it looks interesting and they’re not sure what it does.

After prompting, they said they would probably poke around the entire interface since they’d never used it before, and would want to be familiar with it before solving any particular task.

2. Changing workspaces configurations

They immediately chose Multitasking, since that seems to fit well.

They might also look under Applications, but they’re still unsure what that category is, and they’re intrigued by it.

They also wondered what Universal Access is.

3. Changing keyboard layout

They’d look in the Keyboard section first, and then in the Applications section if they couldn’t find it.

They want to look in the Applications section so they can run an app specific to solving this problem.

After some prompting, they agreed that Region & Language makes sense. They couldn’t work out why they originally skipped it.
Maybe it’s because it’s at the bottom of the list? Or they were wondering if they skipped it because the other categories in that section (Universal Access, Users, etc.) don’t seem as important?

4. Turning locations tracking off

After a short pause, they chose Privacy — they said that “you having control over your tracking affects your privacy”.

5. Screen time

They immediately chose Displays, but said “but I think it might be something else”.

Then they chose Notifications, because the icon for it is a bell, which they thought might relate to reminders.

After some prompting, they said they would never have looked in Screen Time. They think Screen Time would be about shutdown settings, controlled by the system administrator.
In a previous job, they used a computer system with a 15 minute warning that the system was going to shut down (triggered remotely), and Screen Time reminded them of that.

They said that after some thought, Screen Time *would* fit, but they thought the icon for it was unhelpful. They said the icon made them think of a progress spinner or
the Twitter character limit indicator. They wondered if a clock would be better, or putting the existing icon inside a surround that looked like a computer monitor. Or
maybe adding a ‘30s’ or ‘60s’ label in the icon to indicate a countdown was occurring.

6. Revoking camera access from some application

They said this “is a complete new one for me”.

They wondered about going to Displays. Failing that, they would try Privacy.

They were also drawn to the Applications section, since they still had no idea what it was for.

7. Universal Access

They were initially drawn to the Notifications section, since they thought bleeps are a way of notifying the user.

Then they thought about the Displays section, because they want to make the display flash.

They also wondered about Sound, for similar reasons.

After some prompting, they said that Universal Access does make sense, after I had explained it to them. They might have worked it out had they explored the entire settings interface when they first opened it.

8. Changing password

They immediately chose Users, because they are a user, and they want to change their user profile.

9. Screen lock

They were unsure, and thought about choosing Online Accounts — the word ‘accounts’ indicated to them that it might be about changing access to their account.

They then thought about Users, similarly to their answer for question 8.

After some prompting, they said they would never have chosen Privacy, because to them, ‘privacy’ implies privacy policies and safeguarding.

## Discussion points after the scenarios

 * The scenarios got them thinking. A few of the tasks they were asked to do were new and unfamiliar to them.
 * Feedback about the test setup: they thought it would be good to have printouts of what’s in each section, because:
 * They’re a very visual person, and would naturally explore all the sections when they first started the Control Center, so they could visualise access to it in future.
 * For each scenario, they were (in their head) visualising performing the same task on their current computer and then trying to apply it to the printout in front of them.

## Summary

| Task                                          | Result             |
|-----------------------------------------------|--------------------|
| Keyboard shortcuts                            | :x:                |
| Changing workspaces configurations            | :white_check_mark: |
| Changing keyboard layout                      | :x:                |
| Turning locations tracking off                | :white_check_mark: |
| Screen time                                   | :x:                |
| Revoking camera access from some application  | :question:         |
| Universal Access                              | :x:                |
| Changing password                             | :white_check_mark: |
| Screen lock                                   | :x:                |
