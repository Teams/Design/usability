# Participant 4 (M), 2019-11-21
## Background

How many desktop computers do you use on a regular basis? 1 (Windows 10)

What they use the computer for:

- Office applications
- Photo processing and management
- Listening to music
- Email
- Programming or software development
- Watching videos

Phones/tablets: 1 × iPhone 7

Occupation: SEO/website optimisation

## Testing notes

I varied the order of scenarios this time, asking them in the order: 9, 8, 7, 6, 5, 3, 4, 2, 1

## Exercise

1. Keyboard shortcuts

Immediately chose Keyboard.

2. Changing workspaces configurations

They’d try Displays first, since they were thinking about it from a multi-monitor point of view.

Failing that, they’d look in Multitasking, but they thought that Multitasking was probably more about browser tabs.

3. Changing keyboard layout

They immediately chose Keyboard.

Failing that, they would look in Region & Language.

4. Turning locations tracking off

They’d try Privacy first.

If they didn’t find the setting there, they’d look in Online Accounts. On Windows and Apple products, something similar is where your device is listed so you can track it (like ‘find my device’), so they reasoned that might be where location privacy settings are.

5. Screen time

They immediately chose Screen Time because they use an iPhone, which uses the same terminology.

6. Revoking camera access from some application

They immediately chose Privacy.

They then thought about looking in Applications if they couldn’t find it in Privacy. They were thinking about looking in Applications because they were wondering where peripherals would be listed: they saw the sections for Printers, Mouse & Touchpad, Displays, etc., and had been looking for a ‘Cameras’ section to accompany those. Since no such section existed, they were wondering if they’d find it in Applications.

They did *not* think of looking in Applications to find the game from the scenario.

7. Universal Access

They hesitated, and then questioned what the Background section is for. They couldn’t work that out.

They said that if the setting wasn’t in Background, they’d try the Displays section because the task is to do with flashing the screen.

After some prompting, they said that they had no idea what the Universal Access section was for. When they looked at it before, they had thought the icon looked like a picture of a child, and assumed Universal Access was something to do with child locks or parental controls.

8. Changing password

They immediately chose Privacy.

If the setting wasn’t there, they’d try Users.

9. Screen lock

Slightly unsure. They’d try Users first, followed by Privacy. If neither of them contained the setting, they’d get quite frustrated and try the Power section.

They thought about Users because that’s where Windows puts its per-user settings. Then they thought about Privacy because locking the screen is a security thing.

## Discussion points after the scenarios

 * They are used to using a search function to find what they’re looking for, rather than digging through categories.
 * They commented on the grouping of hardware-related sections (Sound, through to Printers) and thought that a Windows-like ‘device manager’ would make more sense than arbitrarily having sections for some peripherals but not for others.

## Summary

| Task                                          | Result             |
|-----------------------------------------------|--------------------|
| Keyboard shortcuts                            | :white_check_mark: |
| Changing workspaces configurations            | :question:         |
| Changing keyboard layout                      | :question:         |
| Turning locations tracking off                | :white_check_mark: |
| Screen time                                   | :white_check_mark: |
| Revoking camera access from some application  | :white_check_mark: |
| Universal Access                              | :x:                |
| Changing password                             | :question:         |
| Screen lock                                   | :question:         |

