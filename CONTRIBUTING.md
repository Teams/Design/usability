# New Contributor Guide

This document provides information for people who are new to GNOME design and want to contribute to GNOME user research activities.

## Preparation

To contribute to GNOME user research activities, you will first need to do some research and setup. This includes:

* Installing and running a recent version of GNOME, and familiarizing yourself with it. GNOME is typically installed by using a Linux distribution. The [Getting GNOME](https://www.gnome.org/getting-gnome/) page includes links to some of these.
* Familiarizing yourself with basic user research concepts and procedures, such as:
   * the different types of bias
   * representativeness and sampling
   * research ethics and privacy concerns
   * interview design and technique
   * how to conduct user testing
   * familiarity with other research methods, like surveys, card sorting and diary studies
* Learning about the GNOME design team. To do this, you can go through each step in the [GNOME design contribution guide](https://welcome.gnome.org/team/design/).
* Getting to know how the GNOME project works. The GNOME Handbook is recommended for this, in particular the pages on [governance](https://handbook.gnome.org/governance.html), [teams](https://handbook.gnome.org/teams.html), [issue tracking](https://handbook.gnome.org/issues.html) and [reporting](https://handbook.gnome.org/issues/reporting.html), [infrastructure](https://handbook.gnome.org/infrastructure.html) and [release planning](https://handbook.gnome.org/release-planning.html).

## How to get started

The [usability project issue tracker](https://gitlab.gnome.org/Teams/Design/usability/-/issues) is the main place where research tasks can be found. You can also create new issues to post the results of any research that you do.

If you are new to GNOME user research, the [newcomers issues](https://gitlab.gnome.org/Teams/Design/usability/-/issues/?label_name%5B%5D=Newcomers) are the best place to start.

## Get in touch

Prospective Outreachy interns should use the [GNOME Outreachy Matrix channel](https://matrix.to/#/#gnome-outreachy:gnome.org) for questions and discussion. More information on how to use Matrix can be found in the GNOME Handbook.

Other discussion can take place in the [GNOME Design Matrix room](https://matrix.to/#/#design:gnome.org).

