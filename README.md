# GNOME Usability

This repository is used by the design team to track user experience research activities. See the issues for research exercises that we would like to be done, or which are ongoing.
